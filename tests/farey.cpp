#include "fractions.hpp"
#include "gtest.hpp"
#include <iostream>
#include <memory>
#include <random>
#include <vector>

template<typename t_scalar, typename Float>
fraction_t<t_scalar> farey_debug( const Float target_p, size_t const iterations, std::ostream & os ){
	using Frac = fraction_t<t_scalar>;
	// centre target between 0 and 1
	Float target = std::abs( target_p - std::trunc(target_p) );
	os	<< "target_p: " << target_p << '\n'
		<< "target:   " << target << '\n';
	Frac less{0,1};
	Frac more{1,1};
	Frac temp;
	for( size_t i{} ; i < iterations ; ++i ){
		temp = farey(less, more);
		os << i
			<< "\t\"" << less << "\"\t\"" << temp << "\"\t\"" << more << "\""
			<< "\t" << static_cast<Float>(less)
			<< "\t" << static_cast<Float>(temp)
			<< "\t" << static_cast<Float>(more)
			<< "\n";
		if(temp.numerator() < 0 || temp.denominator() < 0){
			throw std::out_of_range("Integer overflow.");
		}
		if( temp > target ){
			more = temp;
		} else {
			less = temp;
		}
	}
	if( (static_cast<Float>(more) - target) > (target - static_cast<Float>(less)) ){
		temp = less;
	} else {
		temp = more;
	}

	temp += Frac{ static_cast<t_scalar>( std::abs(target_p) - target ), 1 };
	os << "Result: " << temp << '\n';
	if( target_p > 0 )
		return temp;
	return Frac{0,1} - temp;
}


/// Test all splits of 16
/// just to ensure there's no obviously problematic points
/// iterations required should be a symmetrical shape
gTEST( farey, basic ){
	using F = long double;
	using I = int64_t;
	using Fr = fraction_t<I>;
	// tuned to minimum iterations needed
	std::vector<std::tuple<int,int,size_t>> tests{
		{  0,  16,  0 }, //
		{  1,  16, 15 }, // ###############
		{  2,  16,  7 }, // #######
		{  3,  16,  7 }, // #######
		{  4,  16,  3 }, // ###
		{  5,  16,  7 }, // #######
		{  6,  16,  4 }, // ####
		{  7,  16,  6 }, // ######
		{  8,  16,  1 }, // #
		{  9,  16,  6 }, // ######
		{ 10,  16,  4 }, // ####
		{ 11,  16,  7 }, // #######
		{ 12,  16,  3 }, // ###
		{ 13,  16,  7 }, // #######
		{ 14,  16,  7 }, // #######
		{ 15,  16, 15 }, // ###############
		{ 16,  16,  0 }, //
	};
	for( auto [numer,denom,iters] : tests ){
		long double target = static_cast<F>(numer) / static_cast<F>(denom);
		Fr expect{ numer, denom };
		expect.simplify();

		auto result = farey_algorithm<I>(target, iters);
		gEXPECT_EQ( expect, result )
			<< "input:  "   << numer << '/' << denom
			<< "\ntarget: " << target
			<< "\nresult: " << static_cast<long double>(result);
	}
}


/// test random rational numbers
/// should be able to exactly match input
gTEST( farey, random ){
	std::mt19937 rng{
		std::default_random_engine{
			std::random_device()()
		}()
	};
	std::uniform_int_distribution< int64_t >
		numer{ -100'000, 100'000 },
		denom{        1, 100'000 };

	size_t iterations{ 100'000 };

	for( int i{} ; i < 1'000 ; ++i ){
		auto N = numer(rng);
		auto D = denom(rng);
		fraction_t<int64_t> expect{ N, D };
		expect.simplify();
		long double target = static_cast<long double>(N) / static_cast<long double>(D);

		auto result = farey_algorithm<int64_t>(target, iterations);
		gASSERT_EQ( expect, result )
			<< "target: " << target
			<< "\nresult: " << static_cast<long double>(result);
	}
}


/// test that algorithm finds fractions for random real numbers within a tolerance
/// pretty permissive as I just want to assert that the algorithm tends relatively well
/// limit to 100 iterations to avoid overflowing numerator/denominator
gTEST( farey, tolerance ){
	std::mt19937 rng{
		std::default_random_engine{
			std::random_device()()
		}()
	};
	std::uniform_real_distribution<long double> range{-1, 1};

	for( int i{} ; i < 1'000'000 ; ++i ){
		auto target = range( rng );

		try {
			auto result = farey_algorithm<int64_t>(target, 100);
			auto gap = std::abs(target - static_cast<long double>(result));

			ASSERT_LT( gap, 1e-2)
				<< "target: " << target << '\n'
				<< "result: " << result << '\n'
				<< "float:  " << static_cast<long double>(result);

		} catch( const std::exception & err ){
			std::clog << "Error: " << err.what() << '\n';
			farey_debug<int64_t>(target, 1'000, std::clog);
		}

	}

}
