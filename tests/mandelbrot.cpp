#include "fractions.hpp"
#include "gtest.hpp"
#include <complex>
#include <iostream>

// stress test of fractions, producing the mandelbrot set fractal

template<typename T>
std::complex<T> square( std::complex<T> const & Z ) {
	return {
		Z.real() * Z.real() - Z.imag() * Z.imag(),
		T{ 2 } * Z.real() * Z.imag()
	};
}

template<typename T>
T length_sq( std::complex<T> const & C ) {
	return C.real() * C.real() + C.imag() * C.imag();
};

const size_t PLOT_SIZE = 25;

gTEST( mad, mandelbrot_fraction ){
	using F = fraction_t<int64_t>;
	for( F y{ -2 }; y < 2; y += F{ 4, PLOT_SIZE } ) {
		for( F x{ -2 }; x < 2; x += F{ 4, PLOT_SIZE } ) {
//			if(y == F{-2} ) std::cout << x << ' ';
//			if(x == F{-2} ) std::cout << y << ' ';
			int iter = 0;
			std::complex<F> c{ x, y }, Z = c;
			for( ; iter < 99; ++iter ) {
				if( x == F{-6,25} && y == F{-18,25} ){ // debug log
					std::clog<<Z
						<<' '<<static_cast<double>(Z.real())
						<<' '<<static_cast<double>(Z.imag())
						<<'\n';
				}
				if( length_sq( Z ) > 4.0l ) {
					break;
				}
			//	Z = ( Z * Z ) + c;
				Z = square(Z) + c;
			}
			if( iter < 10 ){
				std::cout << '0' << iter << ' ';
			} else if( iter < 100 ){
				std::cout << iter << ' ';
			} else {
				std::cout << "__ ";
			}
		}
		std::cout << '\n';
	}
	std::clog<<'\n';
}

gTEST( mad, mandelbrot_double ){
	using F = fraction_t<int64_t>;
	for( F y{ -2 }; y < 2; y += F{ 4, PLOT_SIZE } ) {
		for( F x{ -2 }; x < 2; x += F{ 4, PLOT_SIZE } ) {
//			if(y == F{-2} ) std::cout << x << ' ';
//			if(x == F{-2} ) std::cout << y << ' ';
			int iter = 0;
			std::complex<double> c{
				static_cast<double>(x),
				static_cast<double>(y)
			}, Z = c;
			for( ; iter < 99; ++iter ) {
				if( x == F{-6,25} && y == F{-18,25} ){ // debug log
					std::clog<<Z
						<<' '<<static_cast<double>(Z.real())
						<<' '<<static_cast<double>(Z.imag())
						<<'\n';
				}
				if( length_sq( Z ) > 4. ) {
					break;
				}
			//	Z = ( Z * Z ) + c;
				Z = square(Z) + c;
			}
			if( iter < 10 ){
				std::cout << '0' << iter << ' ';
			} else if( iter < 100 ){
				std::cout << iter << ' ';
			} else {
				std::cout << "__ ";
			}
		}
		std::cout << '\n';
	}
	std::clog<<'\n';
}
