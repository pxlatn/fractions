#include "fractions.hpp"
#include "gtest.hpp"


gTEST( function, construction ) {
	using fraction = fraction_t<int>;
	fraction A{ 5 }, B{ 5, 1 };
	gEXPECT_EQ( A, B );
}


gTEST( function, simplify ) {
	using fraction = fraction_t<int>;
	fraction A{ 5, 10 }, B{ 1, 2 }, C{ 10, 20 }, D{ 50, 100 };
	const fraction expect{ 1, 2 };
	gEXPECT_EQ( expect, A );
	gEXPECT_EQ( expect, B );
	gEXPECT_EQ( expect, C );
	gEXPECT_EQ( expect, D );
	A.simplify();
	B.simplify();
	C.simplify();
	D.simplify();
	gEXPECT_EQ( expect.numerator(), A.numerator() );
	gEXPECT_EQ( expect.numerator(), B.numerator() );
	gEXPECT_EQ( expect.numerator(), C.numerator() );
	gEXPECT_EQ( expect.numerator(), D.numerator() );
	gEXPECT_EQ( expect.denominator(), A.denominator() );
	gEXPECT_EQ( expect.denominator(), B.denominator() );
	gEXPECT_EQ( expect.denominator(), C.denominator() );
	gEXPECT_EQ( expect.denominator(), D.denominator() );
}


gTEST( arithmetic, comparison ) {
	using fraction = fraction_t<int>;
	{  // same denominator - positive
		fraction A{ 3, 4 }, B{ 2, 4 };
		EXPECT_GT( A, B );
		EXPECT_LT( B, A );
	}
	{  // same denominator - negative
		fraction A{ 3, -4 }, B{ 2, -4 };
		EXPECT_GT( B, A );
		EXPECT_LT( A, B );
	}
	{  // same numerator
		fraction A{ 2, 3 }, B{ 2, 2 };
		EXPECT_GT( B, A );
		EXPECT_LT( A, B );
	}
	{ // different
		fraction A{ 2, 3 }, B{ 1, 2 };
		EXPECT_GT( A, B );
		EXPECT_LT( B, A );
	}
	{ // different
		fraction A{ 5, 18 }, B{ 4, 17 };
		EXPECT_GT( A, B );
		EXPECT_LT( B, A );
	}
}

gTEST( arithmetic, multiplication ){
	using fraction = fraction_t<int>;
	{
		fraction A{ 1, 2 }, B{ 1, 4 }, expect{ 1, 8 };
		gEXPECT_EQ( expect, A * B );
		gEXPECT_EQ( expect, B * A );
	}
	{
		fraction A{ 1024, 1 }, B{ 3, 8 }, expect{ 128 * 3, 1 };
		gEXPECT_EQ( expect, A * B );
		gEXPECT_EQ( expect, B * A );
	}
}

gTEST( arithmetic, division ){
	using fraction = fraction_t<int>;
	{
		fraction A{ 1, 2 }, B{ 4, 1 }, expect{ 1, 8 };
		gEXPECT_EQ( expect, A / B );
	}
}

gTEST( arithmetic, addition ){
	using fraction = fraction_t<int>;
	{
		fraction A{ 5, 18 }, B{ 4, 17 }, expect{ 157, 306 };
		gEXPECT_EQ( expect, A + B );
		gEXPECT_EQ( expect, B + A );
	}
}


gTEST( arithmetic, subtraction ){
	using fraction = fraction_t<int>;
	{
		fraction A{ 5, 18 }, B{ 4, 17 }, expect{ 13, 306 };
		gEXPECT_EQ( expect, A - B );
	}
}


gTEST( arithmetic, combined ){
	using F = fraction_t<int64_t>;
	int64_t parts = 417;
	const F total{ 361285, 388419 };
	F accumulator{ 0, 1 }, step = total / F{ parts };
	for( int i = 0; i < parts; ++i ) {
		accumulator += step;
	}
	gEXPECT_EQ( total, accumulator );
}

