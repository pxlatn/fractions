#!/bin/bash

tmp=$(mktemp)

llvm-profdata merge \
	-output "${tmp}" \
	./tests/arithmetic_tests.profraw \
	./tests/basic_tests.profraw \
	./tests/random_tests.profraw

llvm-cov show -use-color \
	-instr-profile "${tmp}" \
	./tests/arithmetic_tests \
	-object ./tests/basic_tests \
	-object ./tests/random_tests \
	| less -RS

llvm-cov report -use-color \
	-instr-profile "${tmp}" \
	./tests/arithmetic_tests \
	-object ./tests/basic_tests \
	-object ./tests/random_tests \

rm -fv "${tmp}"
