test: ninja
	cd build && ctest --output-on-failure

ninja: build cmake
	ninja -C build

cmake: build conan
	cd build && cmake \
		-G Ninja \
		-D CMAKE_MODULE_PATH=$(PWD)/build \
		-D CMAKE_EXPORT_COMPILE_COMMANDS=ON \
		..

conan: build
	cd build && conan install .. --build=missing

build:
	mkdir -pv build

.PHONY: test ninja cmake conan
