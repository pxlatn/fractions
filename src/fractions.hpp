#ifndef _FRACTIONS_HPP
#define _FRACTIONS_HPP

#include <limits>   //is_integer
#include <numeric>  // gcd
#include <ostream>  // ostream
#include <utility>  // move
#include <cmath>    // abs, trunc

// https://en.wikipedia.org/wiki/Fraction_(mathematics)

template<typename t_scalar>
class fraction_t {
	public:
	using scalar = t_scalar;
	static_assert(
		std::numeric_limits<scalar>::is_integer,
		"fraction_t only supports integer types." );

	private:
	scalar N{ 0 };
	scalar D{ 1 };

	public:

	// constructors

	fraction_t() = default;
	explicit fraction_t( scalar input )
		: N{ std::move( input ) }, D{ 1 } {
	}
	fraction_t( scalar num, scalar denom )
		: N{ std::move( num ) }, D{ std::move( denom ) } {
	}

	template<typename T>
	explicit operator T() const {
		return static_cast<T>( N ) / static_cast<T>( D );
	}

	// operations

	// https://en.wikipedia.org/wiki/Euclidean_algorithm
	fraction_t simplify() /* non-const */ {
		scalar divisor = std::gcd( N, D );
		if( divisor > 0 ) {
			N /= divisor;
			D /= divisor;
		}
		return *this;
	}

	scalar const & numerator() const {
		return N;
	}
	scalar const & denominator() const {
		return D;
	}

	// comparisons

	friend bool operator==( fraction_t lhs, fraction_t rhs ) {
		lhs.simplify();
		rhs.simplify();
		return lhs.N == rhs.N && lhs.D == rhs.D;
	}

	friend bool operator!=( fraction_t lhs, fraction_t rhs ) {
		lhs.simplify();
		rhs.simplify();
		return lhs.N != rhs.N || lhs.D != rhs.D;
	}

	// Bodge it!
	bool operator<( fraction_t rhs ) const {
		return static_cast<double>( *this ) <
			   static_cast<double>( rhs );
	}
	template<typename T>
	bool operator<( T rhs ) const {
		return static_cast<T>( *this ) < rhs;
	}

	// Bodge it!
	bool operator>( fraction_t rhs ) const {
		return static_cast<double>( *this ) >
			   static_cast<double>( rhs );
	}
	template<typename T>
	bool operator>( T rhs ) const {
		return static_cast<T>( *this ) > rhs;
	}

	friend fraction_t operator*( fraction_t const & lhs, fraction_t const & rhs ) {
		return fraction_t{ lhs.N * rhs.N, lhs.D * rhs.D }.simplify();
	}
	fraction_t operator *=( fraction_t const & rhs ){
		return *this = *this * rhs;
	}

	friend fraction_t operator/( fraction_t const & lhs, fraction_t const & rhs ) {
		return fraction_t{ lhs.N * rhs.D, lhs.D * rhs.N }.simplify();
	}
	fraction_t operator /=( fraction_t const & rhs ){
		return *this = *this / rhs;
	}

	friend fraction_t operator+( fraction_t const & lhs, fraction_t const & rhs ) {
		return fraction_t{ lhs.N * rhs.D + rhs.N * lhs.D, lhs.D * rhs.D }.simplify();
	}
	fraction_t operator +=( fraction_t const & rhs ){
		return *this = *this + rhs;
	}

	friend fraction_t operator-( fraction_t const & lhs, fraction_t const & rhs ) {
		return fraction_t{ lhs.N * rhs.D - rhs.N * lhs.D, lhs.D * rhs.D }.simplify();
	}
	fraction_t operator -=( fraction_t const & rhs ){
		return *this = *this - rhs;
	}


	friend std::ostream & operator << ( std::ostream & os, fraction_t const & rhs ){
//		if( std::abs( rhs.N ) > std::abs( rhs.D ) ) {
//			os << ( rhs.N / rhs.D ) << "+";
//		}
//		return os << ( rhs.N % rhs.D ) << "/" << rhs.D;
		return os << rhs.N << "/" << rhs.D;
	}

	friend fraction_t farey( fraction_t A, fraction_t B ){
		return {
			A.N + B.N,
			A.D + B.D
		};
	}

};

template<typename t_scalar, typename Float>
fraction_t<t_scalar> farey_algorithm( const Float target_p, size_t const iterations ){
	using Frac = fraction_t<t_scalar>;
	// centre target between 0 and 1
	Float target = std::abs( target_p - std::trunc(target_p) );
	Frac less{0,1};
	Frac more{1,1};
	Frac temp;
	for( size_t i{} ; i < iterations ; ++i ){
		temp = farey(less, more);
		if(temp.numerator() < 0 || temp.denominator() < 0){
			throw std::out_of_range("Integer overflow.");
		}
		if( temp > target ){
			more = temp;
		} else {
			less = temp;
		}
	}
	if( (static_cast<Float>(more) - target) > (target - static_cast<Float>(less)) ){
		temp = less;
	} else {
		temp = more;
	}

	temp += Frac{ static_cast<t_scalar>( std::abs(target_p) - target ), 1 };
	if( target_p > 0 )
		return temp;
	return Frac{0,1} - temp;
}

#endif  // _FRACTIONS_HPP
