[requires]
gtest/1.10.0
benchmark/1.5.0

[generators]
cmake_find_package

# vim: set ft=dosini :#
